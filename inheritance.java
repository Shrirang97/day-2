
abstract class language 
{
	abstract void compiler();
}

class lang_java extends language
{
	private String lang = "JAVA Compiler";
	void compiler()
	{
		System.out.println(lang);
	}
}

class lang_python extends language
{
	private String lang = "Python Interpreter";
	void compiler()
	{
		System.out.println(lang);
	}
}

public class oops
{
	public static void main(String args[])
	{
		lang_python python = new lang_python();
		lang_java java = new lang_java();

		python.compiler();
		java.compiler();
	}
}